package com.moni.repositories

import com.moni.model.Document
import com.moni.model.DocumentsResponse
import com.moni.model.NetResult
import com.moni.model.Score

interface ScoreRepository  {
    suspend fun getScore(dni:String) : NetResult<Score>

    suspend fun saveClient(client: Document) : NetResult<Unit>

    suspend fun getListClient() : NetResult<DocumentsResponse>

    suspend fun deleteClient(client: Document) : NetResult<Unit>
}