package com.moni.repositories

import com.moni.api.ScoreAPI
import com.moni.model.Document
import com.moni.model.DocumentsResponse
import com.moni.model.NetResult
import com.moni.model.Score

class ScoreRepositoryImpl(
    private val scoreAPI: ScoreAPI
) : ScoreRepository {
    override suspend fun getScore(dni: String): NetResult<Score> {
        try {
            val response = scoreAPI.score(dni)
            if (response.isSuccessful) {
                response.body()?.let {
                    return NetResult.Success(it)
                }
            }
            return NetResult.Error(response)
        } catch (e: Exception) {
            return NetResult.ErrorException(e)
        }
    }

    override suspend fun saveClient(client: Document): NetResult<Unit> {
        try {
            val response = scoreAPI.saveClient(URL_BASE_SAVE,client)
            if (response.isSuccessful) {
                response.body()?.let {
                    return NetResult.Success(Unit)
                }
            }
            return NetResult.Error(response)
        } catch (e: Exception) {
            return NetResult.ErrorException(e)
        }
    }

    override suspend fun getListClient(): NetResult<DocumentsResponse> {
        try {
            val response = scoreAPI.getDocuments(URL_BASE_SAVE)
            if (response.isSuccessful) {
                response.body()?.let {
                    return NetResult.Success(it)
                }
            }
            return NetResult.Error(response)
        } catch (e: Exception) {
            return NetResult.ErrorException(e)
        }
    }

    override suspend fun deleteClient(client: Document): NetResult<Unit> {
        try {
            val response = scoreAPI.deleteClient(URL_BASE_SAVE,client)
            if (response.isSuccessful) {
                response.body()?.let {
                    return NetResult.Success(Unit)
                }
            }
            return NetResult.Error(response)
        } catch (e: Exception) {
            return NetResult.ErrorException(e)
        }
    }

    public companion object {
        const val URL_BASE_SAVE = "https://firestore.googleapis.com/v1/projects/wired-torus-98413/databases/(default)/documents/users"
    }
}