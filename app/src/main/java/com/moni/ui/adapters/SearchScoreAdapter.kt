package com.moni.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.moni.databinding.ItemDeviceBinding
import com.moni.model.Document

class SearchScoreAdapter(
    private val listener: Listener,
    private var listDocument: List<Document>,
    private val context: Context
) :
    RecyclerView.Adapter<SearchScoreAdapter.ViewHolder>(), Filterable {

    var deviceFilterList = ArrayList<Document>()
    var characterFilterListOriginal = ArrayList<Document>()

    init {
        deviceFilterList = listDocument as ArrayList<Document>
        characterFilterListOriginal = listDocument as ArrayList<Document>
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    deviceFilterList = listDocument as ArrayList<Document>
                } else {
                    val resultList = ArrayList<Document>()
                    for (row in characterFilterListOriginal) {
                        row.name.let {
                            if (it.contains(charSearch, ignoreCase = true)) {
                                resultList.add(row)
                            }
                        }
                    }
                    listDocument = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = listDocument
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                deviceFilterList = results?.values as ArrayList<Document>
                notifyDataSetChanged()
            }

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder.create(parent)

    override fun getItemCount(): Int {
        return listDocument.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(listDocument[position], listener, context)

    class ViewHolder(private val binding: ItemDeviceBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(document: Document, listener: Listener, context: Context) {
            "${document.fields?.apellido?.stringValue} ${document.fields?.nombre?.stringValue}".also { binding.name.text = it }
            binding.dni.text = document.fields?.dni?.stringValue
            binding.email.text = document.fields?.email?.stringValue
            binding.status.text = document.fields?.status?.stringValue
            binding.delete.setOnClickListener {
                listener.onDeleteItem(document, absoluteAdapterPosition)
            }
        }

        companion object {
            fun create(parent: ViewGroup): ViewHolder {
                val binding = ItemDeviceBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                return ViewHolder(binding)
            }
        }
    }

    interface Listener {
        fun onDeleteItem(document: Document, position: Int)
    }
}