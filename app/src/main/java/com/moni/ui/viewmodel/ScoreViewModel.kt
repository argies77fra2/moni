package com.moni.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.moni.model.Document
import com.moni.model.FieldValue
import com.moni.model.NetResult
import com.moni.repositories.ScoreRepository
import kotlinx.coroutines.launch

class ScoreViewModel(
    private val scoreRepository: ScoreRepository
) : ViewModel() {

    val showMessageScore: MutableLiveData<Boolean> = MutableLiveData()
    val showMessageSaveClient: MutableLiveData<Boolean> = MutableLiveData()
    fun getScore(document: Document){
        viewModelScope.launch {
            when (val response = scoreRepository.getScore(document.fields?.dni?.stringValue.toString())) {
                is NetResult.Success -> {
                    showMessageScore.value = response.data.status == "rejected"
                    // lanzar el guardar data
                    val newStatus= response.data.status
                     document.fields?.status = FieldValue(newStatus)
                     saveClient(document)
                }

                is NetResult.Error, is NetResult.ErrorException -> {

                }
            }
        }
    }

  private fun saveClient(document: Document) {
        viewModelScope.launch {
            when (val response = scoreRepository.saveClient(document)) {
                is NetResult.Success -> {
                    showMessageSaveClient.value =true
                }

                is NetResult.Error, is NetResult.ErrorException -> {
                    showMessageSaveClient.value =false
                }
            }
        }
    }
}