package com.moni.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.moni.model.Document
import com.moni.model.DocumentsResponse
import com.moni.model.NetResult
import com.moni.repositories.ScoreRepository
import kotlinx.coroutines.launch

class ScoreClientViewModel(
    private val scoreRepository: ScoreRepository
) : ViewModel() {

    val showMessageDeleteClient: MutableLiveData<Boolean> = MutableLiveData()

    private val continueMyContentStateLiveData: MutableLiveData<DocumentsResponse?> =
        MutableLiveData()
    fun getClientList(): MutableLiveData<DocumentsResponse?> {
            viewModelScope.launch {
                when (val response = scoreRepository.getListClient()) {
                    is NetResult.Success -> {
                        continueMyContentStateLiveData.postValue(response.data)
                    }

                    is NetResult.Error, is NetResult.ErrorException -> {

                    }
                }
            }
        return continueMyContentStateLiveData
       }

    fun deleteClient(document: Document) {
        viewModelScope.launch {
            when (val response = scoreRepository.deleteClient(document)) {
                is NetResult.Success -> {
                    showMessageDeleteClient.value =true
                }

                is NetResult.Error, is NetResult.ErrorException -> {
                    showMessageDeleteClient.value =false
                }
            }
        }

    }
}