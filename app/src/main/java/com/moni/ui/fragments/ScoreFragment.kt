package com.moni.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.moni.R
import com.moni.databinding.ScoreFragmentBinding
import com.moni.model.Document
import com.moni.model.FieldValue
import com.moni.model.Fields
import com.moni.ui.viewmodel.ScoreViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ScoreFragment : Fragment() {
    private val viewModel by viewModel<ScoreViewModel>()
    private lateinit var binding: ScoreFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ScoreFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonScore.setOnClickListener {
            val fields = Fields(
                nombre = FieldValue(binding.editTextNombre.text.toString()),
                apellido = FieldValue(binding.editTextApellido.text.toString()),
                dni = FieldValue(binding.editTextDNI.text.toString()),
                email = FieldValue(binding.editTextEmail.text.toString()),
                genero = FieldValue(binding.spinnerGenero.selectedItem.toString()),
                status = FieldValue("")
            )
            val document= Document(fields = fields)
            viewModel.getScore(document)
        }
        viewModel.showMessageScore.observe(viewLifecycleOwner, Observer { show ->
            if (show) {
                Toast.makeText(
                    context,
                    getString(R.string.rejected_txt),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    context,
                    getString(R.string.acepted_txt),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

        viewModel.showMessageSaveClient.observe(viewLifecycleOwner, Observer { show ->
            if (show) {
                Toast.makeText(
                    context,
                    getString(R.string.save_client),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    context,
                    getString(R.string.text_error),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

        binding.buttonListado.setOnClickListener {
            val action = ScoreFragmentDirections.actionScoreFragmentToScoreClientFragment()
            Navigation.findNavController(requireView()).navigate(action)
        }

    }

}
