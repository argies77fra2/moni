package com.moni.ui.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.moni.R
import com.moni.databinding.ScoreClientFragmentBinding
import com.moni.model.Document
import com.moni.ui.adapters.SearchScoreAdapter
import com.moni.ui.viewmodel.ScoreClientViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ScoreClientFragment : Fragment(), SearchScoreAdapter.Listener {
    private val viewModel by viewModel<ScoreClientViewModel>()
    private lateinit var binding: ScoreClientFragmentBinding
    private var adapterDevice: SearchScoreAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ScoreClientFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getClientList()


        binding.recyclerDevices.layoutManager = LinearLayoutManager(context)
        binding.recyclerDevices.hasFixedSize()

        setAdapterDevices()

        binding.device.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                adapterDevice!!.filter.filter(s)
                binding.recyclerDevices.isVisible = true
            }
        })

        viewModel.showMessageDeleteClient.observe(viewLifecycleOwner, Observer { show ->
            if (show) {
                Toast.makeText(
                    context,
                    getString(R.string.delete_client),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    context,
                    getString(R.string.text_error),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

    }

    private fun setAdapterDevices() {
        viewModel.getClientList().observe(
            viewLifecycleOwner
        ) { resource ->
            binding.loading.progress.visibility = View.GONE
            resource?.documents.let { documents ->
                val filterDocuments = documents?.filter { it.fields?.dni != null }
                adapterDevice =
                    filterDocuments?.let {
                        SearchScoreAdapter(
                            this,
                            it.toList(), requireContext()
                        )
                    }
                binding.recyclerDevices.adapter = adapterDevice
            }
        }
    }

    override fun onDeleteItem(document: Document, position: Int) {
        viewModel.deleteClient(document)
    }
}
