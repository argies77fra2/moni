package com.moni


import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.moni.di.appModule
import com.moni.di.moniApiModule
import com.moni.di.networkModule
import com.moni.di.repositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MoniApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
        // Start Koin
        startKoin{
            androidLogger(Level.NONE)
            androidContext(this@MoniApp)
            modules(appModule, repositoryModule, networkModule, moniApiModule)

        }
    }
}