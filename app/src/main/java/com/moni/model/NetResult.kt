package com.moni.model

import retrofit2.Response

sealed class NetResult<out T> {

    data class Success<out T>(val data: T) : NetResult<T>()
    data class Error(val error: Response<*>, var code: Int? = null) : NetResult<Nothing>()
    data class ErrorException(val error: Throwable, var code: Int? = null) : NetResult<Nothing>()
}