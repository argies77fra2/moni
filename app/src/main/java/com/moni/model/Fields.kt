package com.moni.model

class Fields(
    val nombre: FieldValue? =null,
    val apellido: FieldValue? = null,
    val dni: FieldValue? = null,
    val email: FieldValue? = null,
    val genero: FieldValue? = null,
    var status: FieldValue? = null
)
