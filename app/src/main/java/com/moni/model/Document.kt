package com.moni.model


data class Document (
    val name: String ="",
    var fields: Fields?,
    val createTime: String ="",
    val updateTime: String =""
)