package com.moni.model

import com.google.gson.annotations.SerializedName

class DocumentsResponse(
    @SerializedName("documents") val documents: MutableList<Document> = mutableListOf(),
    @SerializedName("name") val name: String
)