package com.moni.di

import android.content.Context
import com.moni.AppExecutors
import com.moni.ui.viewmodel.ScoreClientViewModel
import com.moni.ui.viewmodel.ScoreViewModel
import com.moni.ui.viewmodel.SplashViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private const val SP_FILE = "user_preferences"

@JvmField
val appModule = module {

    single { AppExecutors() }

    viewModel{ ScoreClientViewModel(get()) }
    viewModel{ ScoreViewModel(get()) }
    viewModel{ SplashViewModel() }
    single { androidContext().getSharedPreferences(SP_FILE, Context.MODE_PRIVATE) }

}

