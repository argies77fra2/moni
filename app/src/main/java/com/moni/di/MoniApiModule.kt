package com.moni.di

import com.moni.api.ScoreAPI
import org.koin.dsl.module
import retrofit2.Retrofit

val moniApiModule = module {

    fun provideScoreApi(retrofit: Retrofit): ScoreAPI {
        return retrofit.create(ScoreAPI::class.java)
    }
    single { provideScoreApi(get())}
}