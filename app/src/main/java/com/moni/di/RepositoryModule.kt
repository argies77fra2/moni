package com.moni.di


import com.moni.repositories.ScoreRepository
import com.moni.repositories.ScoreRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single<ScoreRepository> { ScoreRepositoryImpl(get()) }
}

