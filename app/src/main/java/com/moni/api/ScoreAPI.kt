package com.moni.api

import com.moni.model.Document
import com.moni.model.DocumentsResponse
import com.moni.model.Score
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Url

interface ScoreAPI {
    @GET("api/v4/scoring/pre-score/{dni}")
    @Headers(
        "Accept: application/json",
        "Content-Type: application/json"
    )
    suspend fun score(
        @Path(
            value = "dni",
            encoded = true
        ) dni: String
    ): Response<Score>

    @POST
    @Headers(
        "Accept: application/json",
        "Content-Type: application/json"
    )
    suspend fun saveClient(
        @Url url: String,
        @Body document: Document
    ): Response<DocumentsResponse>


    @GET
    @Headers(
        "Accept: application/json",
        "Content-Type: application/json"
    )
    suspend fun getDocuments(
        @Url url: String
    ): Response<DocumentsResponse>

    @DELETE
    @Headers(
        "Accept: application/json",
        "Content-Type: application/json"
    )
    suspend fun deleteClient(
        @Url url: String,
        @Body document: Document
    ): Response<DocumentsResponse>



}